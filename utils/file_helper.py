
def get_int_array_from_file(filename):
    """Read strings from file and convert them to integers.
    :filename path to file
    :return returns array of integers"""
    with open(filename, 'r') as file:
        lines = file.readlines()
    return [int(x) for x in lines]
