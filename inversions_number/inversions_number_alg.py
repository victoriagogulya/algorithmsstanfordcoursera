import operator
from utils import file_helper as fhelp


def _merge(left, right, compare):
    """Merge two subarrays in a single sorted subarray and calculate ammount of splitted inversions
    :return arr, inversion_count"""
    result = []
    ninv = 0
    i, j = 0, 0
    while i < len(left) and j < len(right):
        if compare(left[i], right[j]):
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
            ninv += len(left) - i
    while i < len(left):
        result.append(left[i])
        i += 1
    while j < len(right):
        result.append(right[j])
        j += 1
    return result, ninv


def merge_sort(arr, compare=operator.lt):
    """Standart Merge sort and counting inversions
    :return arr, inversion_count"""
    if len(arr) < 2:
        return arr[:], 0
    else:
        mid = int(len(arr) / 2)
        left, ninv_left = merge_sort(arr[:mid], compare)
        right, ninv_right = merge_sort(arr[mid:], compare)
        result, ninv_split = _merge(left, right, compare)
        return result, ninv_left + ninv_right + ninv_split


if __name__ == '__main__':
    path = "IntegerArray.txt"
    arr = fhelp.get_int_array_from_file(path)
    arr, ninv = merge_sort(arr)
    print('Inversions number:', ninv)

