
def _add_zeroz(val, nzeros, left=True):
    if not isinstance(val, str):
        val = str(val)
    if left:
        return val.zfill(nzeros + len(val))
    else:
        return val.ljust(nzeros + len(val), '0')


def karatsuba_multi(x, y):
    x_str = str(x)
    y_str = str(y)
    x_len = len(x_str)
    y_len = len(y_str)

    if x_len == 1 and y_len == 1:
        return x * y
    elif x_len < y_len:
        x_str = _add_zeroz(x_str, y_len - x_len)
    elif y_len < x_len:
        y_str = _add_zeroz(y_str, x_len - y_len)
    if x_len % 2 != 0:
        _add_zeroz(x_str, x_len + 1)
        _add_zeroz(y_str, y_len + 1)
    x_len = len(x_str)
    y_len = len(y_str)

    mid = x_len // 2 if x_len % 2 == 0 else x_len // 2 + 1
    a = int(x_str[:mid])
    b = int(x_str[mid:])
    c = int(y_str[:mid])
    d = int(y_str[mid:])

    ac = karatsuba_multi(a, c)
    bd = karatsuba_multi(b, d)
    abcd = karatsuba_multi(a + b, c + d)

    z0 = int(_add_zeroz(str(ac), (x_len - mid) * 2, False))
    z1 = int(_add_zeroz(str(abcd - ac - bd), x_len - mid, False))
    return z0 + z1 + bd


if __name__ == '__main__':
    x = 3141592653589793238462643383279502884197169399375105820974944592
    y = 2718281828459045235360287471352662497757247093699959574966967627

    expect = x * y
    actual = karatsuba_multi(x, y)
    print('isOK=', expect == actual)
    print('expect=', expect)
    print('actual=', actual)


