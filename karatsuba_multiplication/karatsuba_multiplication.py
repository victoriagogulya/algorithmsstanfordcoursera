
def karatsuba_multi(x, y):
    if x < 10 or y < 10:
        return x * y

    n = max(len(str(x)), len(str(y)))
    k = n // 2

    a = int(x // 10 ** k)
    b = int(x % 10 ** k)
    c = int(y // 10 ** k)
    d = int(y % 10 ** k)

    z0 = karatsuba_multi(a, c)
    z1 = karatsuba_multi(b, d)
    z2 = karatsuba_multi(a + b, c + d) - z0 - z1

    return int(z0 * 10 ** (k * 2) + z2 * 10 ** k + z1)


if __name__ == '__main__':
    x = 3141592653589793238462643383279502884197169399375105820974944592
    y = 2718281828459045235360287471352662497757247093699959574966967627
    expect = x * y
    actual = karatsuba_multi(x, y)
    print('isOK=', expect == actual)
    print('expect=', expect)
    print('actual=', actual)


